'use strict'

require('array.prototype.flatmap').shim()
const csvFilePath = '/home/amulya/Downloads/aasample.csv';
const csv = require('csvtojson');
const { Client } = require('@elastic/elasticsearch')
const client = new Client({
  node: 'http://localhost:9200'
})

const readFile = async () => {
    return new Promise((resolve, reject) => {
        csv().fromFile(csvFilePath).then((jsonObj) => {
            resolve(jsonObj);
        }).catch((e) => {
            console.error(e);
            reject(e);
        })
    })

}

async function run() {
    const dataset = await readFile();
    // process.exit(0);

    const body = dataset.flatMap(doc => [{ index: { _index: 'crm-data' } }, doc])

    const { body: bulkResponse } = await client.bulk({ refresh: true, body })

    if (bulkResponse.errors) {
        const erroredDocuments = []
        // The items array has the same order of the dataset we just indexed.
        // The presence of the `error` key indicates that the operation
        // that we did for the document has failed.
        bulkResponse.items.forEach((action, i) => {
            const operation = Object.keys(action)[0]
            if (action[operation].error) {
                erroredDocuments.push({
                    // If the status is 429 it means that you can retry the document,
                    // otherwise it's very likely a mapping error, and you should
                    // fix the document before to try it again.
                    status: action[operation].status,
                    error: action[operation].error,
                    operation: body[i * 2],
                    document: body[i * 2 + 1]
                })
            }
        })
        console.log("error docs : ", erroredDocuments, "\n\n")
    }

    const { body: count } = await client.count({ index: 'crm-data' })
    console.log("count of all docs : ", count, '\n\n')
}

run().catch(console.log)